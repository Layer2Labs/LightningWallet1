
## Lightning Wallet
     
- 🪙 Lightning Wallet: BTC
- 💵 Fiat gateway: USD, EUR, RUB, UAH... via itez.com , Simplex
- ⚛️ P2P exchange – atomic swaps by Portico Exchange (https://github.com/PorticoExchange/PorticoExchangeFrontendV2)
- 💡 Open-source, client-side
- 📦 Embeddable into your site!
  
Live version: soon. 

