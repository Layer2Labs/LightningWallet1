(draft)
```
Todo:
- default withdraw fee = ?%
- swaponline.io withdraw fee = ?%
```

# Fees

## Miner fee

Needed for every transaction. Defined by third party miners, not configurable.


## Service fee

### Service fee for exchange

Currently = 0.004, will be customizable soon (work in progress)


### Service fee for withdrawal

Configurable in config files / WordPress panel:
- set 0.00001%
- set admin address to receive fees
